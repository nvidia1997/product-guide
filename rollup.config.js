const packageJson = require("./package.json");
import commonjs from '@rollup/plugin-commonjs';
import image from '@rollup/plugin-image';
import resolve from "@rollup/plugin-node-resolve";
import { RollupOptions } from "rollup";
import cleaner from 'rollup-plugin-cleaner';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import postcss from 'rollup-plugin-postcss';
import typescript from 'rollup-plugin-typescript2';
import { uglify } from "rollup-plugin-uglify";
import { generateScopedName } from "./rollup.utils";

/**
 * @param {Object} commandLineArgs
 * @returns {RollupOptions}
 */
export default (commandLineArgs) => {
  return {
    input: 'src/components/product-guide/index.ts',
    output: [
      {
        file: packageJson.main,
        format: "es",
        sourcemap: false,
      },
    ],
    plugins: [
      cleaner({
        targets: ['dist'],
      }),
      peerDepsExternal(),
      resolve(),
      commonjs(),
      typescript({ useTsconfigDeclarationDir: true }),
      image(),
      postcss({
        extract: false,
        modules: {
          generateScopedName: generateScopedName,
        },
      }),
      uglify(),
    ],
  };
}
