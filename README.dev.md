## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm start

# compiles the project
npm run rollup-compile

# starts compilation in watch mode(recompiles on changes)
npm run rollup-compile-watch
```
