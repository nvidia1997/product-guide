import {createContext} from "react";
import {AppGuideContextValue} from "./product-guide.typedef";

// @ts-ignore
export const AppGuideContext = createContext<AppGuideContextValue>(undefined);
