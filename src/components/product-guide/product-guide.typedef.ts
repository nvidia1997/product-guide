export interface AppGuideStepActionArgs {
    stepIndex: number,
}

export interface AppGuideStep {
    title?: any,
    selector?: string,
    content: any,
    action?: (args: AppGuideStepActionArgs) => void
}

export enum VerticalPosition {
    Top,
    Center,
    Bottom
}

export enum HorizontalPosition {
    Left,
    Right
}

export interface AppGuideContextValue {
    steps: AppGuideStep[],
    activeStepIndex: number,
    setActiveStepIndex: (stepIndex: number) => void,
    palette?: string,
    onClose: () => void,
    closeOnMaskClick?: boolean,
    verticalPosition: VerticalPosition,
    horizontalPosition: HorizontalPosition,
}
