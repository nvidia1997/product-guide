import PropTypes from "prop-types";
import React, {useMemo, useState} from "react";
import Guide from "./components/guide";
import {AppGuideContext} from "./product-guide.constants";
import {AppGuideContextValue, AppGuideStep, HorizontalPosition, VerticalPosition} from "./product-guide.typedef";

type Props = {
    palette?: string,
    steps: AppGuideStep[],
    isOpen: boolean,
    closeOnMaskClick?: boolean,
    onClose: () => void,
    verticalPosition?: VerticalPosition,
    horizontalPosition?: HorizontalPosition,
    activeStepIndex?: number
    setActiveStepIndex?: (stepIndex: number) => void
}

function ProductGuide(props: Props) {
    const {
        palette,
        steps = [],
        isOpen,
        onClose,
        closeOnMaskClick = false,
        verticalPosition = VerticalPosition.Bottom,
        horizontalPosition = HorizontalPosition.Left,
        activeStepIndex: activeStepIndex_Props,
        setActiveStepIndex: setActiveStepIndex_Props,
    } = props;

    const hasValidStepControls = useMemo(() => {
        const _undefinedCount = [
            activeStepIndex_Props,
            setActiveStepIndex_Props
        ]
            .filter(x => x === undefined || x === null)
            .length;

        return _undefinedCount === 0 || _undefinedCount === 2;
    }, [activeStepIndex_Props, setActiveStepIndex_Props]);

    if (!hasValidStepControls) {
        throw new Error("If you're planning to manually control step index, please provide both 'activeStepIndex' and 'setActiveStepIndex'");
    }

    const [activeStepIndex_State, setActiveStepIndex_State] = useState(0);

    const activeStepIndex = typeof activeStepIndex_Props === "number"
        ? activeStepIndex_Props
        : activeStepIndex_State;

    const setActiveStepIndex = typeof setActiveStepIndex_Props === "function"
        ? setActiveStepIndex_Props
        : setActiveStepIndex_State;

    const appGuideContextValue: AppGuideContextValue = {
        steps: steps,
        activeStepIndex: activeStepIndex,
        setActiveStepIndex,
        palette: palette,
        onClose: onClose,
        closeOnMaskClick: closeOnMaskClick,
        verticalPosition: verticalPosition,
        horizontalPosition: horizontalPosition,
    };

    if (!isOpen) {
        return null;
    }

    return (
        <AppGuideContext.Provider value={appGuideContextValue}>
            <Guide/>
        </AppGuideContext.Provider>
    );
}

ProductGuide.propTypes = {
    palette: PropTypes.string,
    activeStepIndex: PropTypes.number,
    setActiveStepIndex: PropTypes.func,
    closeOnMaskClick: PropTypes.bool,
    verticalPosition: PropTypes.oneOf(
        Object.values(VerticalPosition).filter(x => typeof x === 'number')
    ),
    horizontalPosition: PropTypes.oneOf(
        Object.values(HorizontalPosition).filter(x => typeof x === 'number')
    ),
    steps: PropTypes.arrayOf(PropTypes.exact({
        title: PropTypes.string,
        selector: PropTypes.string,
        content: PropTypes.any.isRequired,
        action: PropTypes.func
    }).isRequired).isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
};

export default ProductGuide;
