import React, {CSSProperties} from "react";
import clsx from "clsx";
// @ts-ignore
import styles from "./iconButton.module.scss";

type Props = {
    icon?: string | (() => any),
    backgroundImage?: string,
    onClick?: (e: MouseEvent) => void,
    disabled?: boolean,
    enableDefaultHoverClasses?: boolean,
    classes?: string,
    width?: string,
    height?: string,
    title?: string,
    active?: boolean,
    containerClasses?: string
    style?: CSSProperties
}

const IconButton = (props: Props) => {
    const {
        icon, backgroundImage, onClick,
        disabled = false,
        active, enableDefaultHoverClasses = true,
        classes = "", title,
        width, height, containerClasses = "",
        style = {},
    } = props;

    const _imgParams = {
        className: clsx({
            [classes]: !!classes,
            [styles.iconButton]: true,
            [styles.disabled]: disabled,
            [styles.defaultHover]: enableDefaultHoverClasses,
        }),
        style: {
            ...style,
            width,
            height,
        },
        role: onClick ? "button" : undefined,
        onClick: disabled ? () => null : onClick,
        title
    };

    const _renderBase64 = () => {
        return (
            // @ts-ignore
            <img
                {..._imgParams}
                alt="icon"
                width={width}
                height={height}
                src={backgroundImage}
            />
        );
    };

    const _renderComponent = () => {
        return (
            // @ts-ignore
            <span
                {..._imgParams}
                style={{
                    ...style,
                    ..._imgParams.style,
                    backgroundImage: `url(${backgroundImage})`,
                }}
            >
        {typeof icon === "function" && icon()}
    </span>
        );
    };

    return (
        <span
            className={clsx({
                [styles.iconButtonContainer]: true,
                [containerClasses]: !!containerClasses,
                [styles.active]: !!active,
            })}
        >
      {
          backgroundImage?.startsWith("data")
              ? _renderBase64()
              : _renderComponent()
      }
    </span>
    );
};

export default IconButton;
