import {HorizontalPosition, VerticalPosition} from "../../product-guide.typedef";

export interface NavigatorPositionConfig {
    horizontalPosition: HorizontalPosition,
    verticalPosition: VerticalPosition
}

export interface CalculateTopOffsetArgs {
    verticalPosition: VerticalPosition,
    highlightWindowBoundingRect: DOMRect,
    navigatorBoundingRect: DOMRect,
}

export interface CalculateNavigatorPosStylesArgs {
    navigatorElement: HTMLElement | null,
    highlightWindowElement?: HTMLElement | null,
    verticalPosition: VerticalPosition,
    horizontalPosition: HorizontalPosition
}

export interface CalculateLeftOffsetArgs {
    horizontalPosition: HorizontalPosition,
    verticalPosition: VerticalPosition,
    highlightWindowBoundingRect: DOMRect,
    navigatorBoundingRect: DOMRect,
}

export interface ViewPortRect {
    top: number,
    bottom: number,
    left: number,
    right: number
}
