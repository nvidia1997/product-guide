import clsx from "clsx";
import React, {createRef, CSSProperties, useCallback, useContext, useEffect, useState} from "react";
import {AppGuideContext} from "../../product-guide.constants";
import {AppGuideContextValue} from "../../product-guide.typedef";
import Navigator from "./components/navigator";
// @ts-ignore
import styles from "./guide.module.scss";
import {createHighlightWindowPosStyles, createNavigatorPosStyles} from "./guide.utils";

const navigatorRef = createRef<HTMLElement>();
const highlightWindowRef = createRef<HTMLElement>();

function Guide() {
    const {
        closeOnMaskClick,
        activeStepIndex,
        setActiveStepIndex,
        steps,
        onClose,
        verticalPosition,
        horizontalPosition
    } = useContext<AppGuideContextValue>(AppGuideContext);

    const [highlightWindowStyles, setHighlightWindowStyles] = useState<CSSProperties>();
    const [navigatorStyles, setNavigatorStyles] = useState<CSSProperties>();

    const setHighlightWindowPosStyles = (targetElement: HTMLElement) => {
        setHighlightWindowStyles(createHighlightWindowPosStyles(targetElement));
    };

    const setNavigatorPosStyles = useCallback(() => {
        const _styles = createNavigatorPosStyles({
            navigatorElement: navigatorRef.current,
            highlightWindowElement: highlightWindowRef.current,
            horizontalPosition,
            verticalPosition
        });

        setNavigatorStyles(_styles);
    }, [horizontalPosition, verticalPosition]);

    const updateView = useCallback((nextStepIndex = activeStepIndex) => {
        const _nextStep = steps[nextStepIndex];
        if (!_nextStep) {
            return;
        }

        if (_nextStep.action) {
            _nextStep.action({stepIndex: nextStepIndex});
        }

        const _updateView = (targetElement: HTMLElement) => {
            setTimeout(() => {
                targetElement.scrollIntoView();

                setHighlightWindowPosStyles(targetElement);
                setNavigatorPosStyles();
            }, 0); // small trick to execute code synchronously
        };

        if (_nextStep.selector) {
            let targetElement = document.querySelector(_nextStep.selector);

            if (targetElement) {
                _updateView(targetElement as HTMLElement);
            }
            else {
                const observer = new MutationObserver(() => {
                    // @ts-ignore
                    targetElement = document.querySelector(_nextStep.selector);
                    if (!targetElement) {
                        return;
                    }

                    if (document.contains(targetElement)) {
                        _updateView(targetElement as HTMLElement);

                        observer.disconnect();
                    }
                });

                observer.observe(document, {
                    attributes: true,
                    childList: true,
                    characterData: true,
                    subtree: true,
                });
            }
        }

        setActiveStepIndex(nextStepIndex);
    }, [activeStepIndex, setActiveStepIndex, setNavigatorPosStyles, steps]);

    // eslint-disable-next-line
    useEffect(updateView, []);

    useEffect(() => {
        const onResize = () => updateView();
        window.addEventListener("resize", onResize);

        return () => {
            window.removeEventListener("resize", onResize);
        };
    }, [updateView]);

    const _onNextStepClick = () => {
        let _nextStepIndex = activeStepIndex + 1;
        if (activeStepIndex + 1 === steps.length) {
            onClose();
        }

        _nextStepIndex = Math.min(_nextStepIndex, steps.length - 1);

        if (_nextStepIndex === activeStepIndex) {
            return;
        }

        updateView(_nextStepIndex);
    };

    const _onPrevStepClick = () => {
        const _prevStepIndex = Math.max(0, activeStepIndex - 1);
        if (_prevStepIndex === activeStepIndex) {
            return;
        }

        updateView(_prevStepIndex);
    };

    return (
        <div
            onClick={closeOnMaskClick ? onClose : undefined}
            className={clsx({
                [styles.wall]: true,
                [styles.hiddenWall]: !Boolean(highlightWindowStyles),
            })}
        >
            <div className={styles.frame}>
                <div
                    // @ts-ignore
                    ref={highlightWindowRef}
                    onClick={(e: React.MouseEvent) => e.stopPropagation()}
                    className={styles.highlightWindow}
                    style={highlightWindowStyles}
                />

                <Navigator
                    ref={navigatorRef}
                    style={navigatorStyles}
                    onNextStepClick={_onNextStepClick}
                    onPrevStepClick={_onPrevStepClick}
                    updateView={updateView}
                />
            </div>
        </div>
    );
}

export default Guide;
