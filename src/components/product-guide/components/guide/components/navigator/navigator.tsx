import clsx from "clsx";
import React, {CSSProperties, forwardRef, useContext} from "react";
// @ts-ignore
import CloseIcon from "../../../../icons/close.svg";
// @ts-ignore
import FinishIcon from "../../../../icons/finish.svg";
// @ts-ignore
import LeftArrowIcon from "../../../../icons/left-arrow.svg";
// @ts-ignore
import RightArrowIcon from "../../../../icons/right-arrow.svg";
import {AppGuideContext} from "../../../../product-guide.constants";
import {AppGuideContextValue} from "../../../../product-guide.typedef";
import stlx from "../../../../utils/stlx.utils";
import IconButton from "../../../icon-button";
// @ts-ignore
import styles from "./navigator.module.scss";

type  Props = {
    style: CSSProperties | undefined,
    onNextStepClick: () => void,
    onPrevStepClick: () => void,
    updateView: (nextStepIndex: number) => void,
}
const Navigator = forwardRef((props: Props, ref) => {
    const {
        style,
        onNextStepClick,
        onPrevStepClick,
        updateView,
    } = props;

    const {
        activeStepIndex,
        palette,
        steps,
        onClose
    } = useContext<AppGuideContextValue>(AppGuideContext);

    const activeStep = steps[activeStepIndex];

    return (
        <div
            // @ts-ignore
            ref={ref}
            className={styles.navigator}
            style={style}
            onClick={(e: React.MouseEvent) => e.stopPropagation()}
        >
            <div className={styles.topBar}>
                <div
                    className={styles.topBarTitle}
                    title={
                        typeof activeStep?.title === "string"
                            ? activeStep.title
                            : undefined
                    }
                >
                    {
                        typeof activeStep?.title === "function"
                            ? activeStep.title()
                            : activeStep?.title
                    }
                </div>
                <IconButton
                    classes={styles.closeBtn}
                    backgroundImage={CloseIcon}
                    width="11px"
                    height="11px"
                    onClick={onClose}
                />
            </div>

            <div className={styles.content}>
                {
                    typeof activeStep?.content === "function"
                        ? activeStep.content()
                        : activeStep?.content
                }
            </div>

            <div className={styles.navBar}>
                <IconButton
                    backgroundImage={LeftArrowIcon}
                    onClick={onPrevStepClick}
                    disabled={activeStepIndex === 0}
                />

                <div className={styles.stepPages}>
                    {steps.map((step, stepIndex) =>
                        <IconButton
                            key={`step-${stepIndex}`}
                            classes={clsx({
                                [styles.step]: true,
                                [styles.activeStep]: stepIndex === activeStepIndex,
                            })}
                            style={stlx({
                                [`background-color: ${palette}`]: stepIndex === activeStepIndex,
                            })}
                            width="10px"
                            height="10px"
                            onClick={() => updateView(stepIndex)}
                            title={`${stepIndex + 1}`}
                        />
                    )}
                </div>

                <IconButton
                    backgroundImage={
                        steps.length - 1 === activeStepIndex
                            ? FinishIcon
                            : RightArrowIcon
                    }
                    onClick={onNextStepClick}
                />
            </div>
        </div>
    );
});

export default Navigator;
