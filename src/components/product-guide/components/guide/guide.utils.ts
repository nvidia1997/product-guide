import {CSSProperties} from "react";
import {HorizontalPosition, VerticalPosition} from "../../product-guide.typedef";
import {elementOffset, navigatorPositionConfigs} from "./guide.constants";
import {
    CalculateLeftOffsetArgs,
    CalculateNavigatorPosStylesArgs,
    CalculateTopOffsetArgs,
    ViewPortRect
} from "./guide.typedef";

function calculateNavigatorLeftPos(args: CalculateLeftOffsetArgs): number {
    if (args.verticalPosition === VerticalPosition.Center) {
        if (args.horizontalPosition === HorizontalPosition.Left) {
            return args.highlightWindowBoundingRect.left - args.navigatorBoundingRect.width - elementOffset;
        }
        else {
            return args.highlightWindowBoundingRect.right + elementOffset;
        }
    }
    else {
        if (args.horizontalPosition === HorizontalPosition.Left) {
            return args.highlightWindowBoundingRect.left;
        }
        else {
            return args.highlightWindowBoundingRect.left + args.highlightWindowBoundingRect.width - args.navigatorBoundingRect.width;
        }
    }
}

function calculateNavigatorTopPos(args: CalculateTopOffsetArgs): number {
    if (args.verticalPosition === VerticalPosition.Center) {
        return args.highlightWindowBoundingRect.top;
    }

    if (args.verticalPosition === VerticalPosition.Top) {
        return args.highlightWindowBoundingRect.top - args.navigatorBoundingRect.height - elementOffset;
    }
    else {
        return args.highlightWindowBoundingRect.bottom + elementOffset;
    }
}

function calculateNavigatorPosStyles(
    navigatorBoundingRect: DOMRect,
    highlightWindowBoundingRect: DOMRect,
    horizontalPosition: HorizontalPosition,
    verticalPosition: VerticalPosition,
): CSSProperties {
    const _styles: CSSProperties = {};

    _styles.top = calculateNavigatorTopPos({
        verticalPosition,
        navigatorBoundingRect,
        highlightWindowBoundingRect,
    }) + window.scrollY;

    _styles.left = calculateNavigatorLeftPos({
        verticalPosition,
        horizontalPosition,
        highlightWindowBoundingRect,
        navigatorBoundingRect
    }) + window.scrollX;

    return _styles;
}

function createViewPortRect(styles: CSSProperties, navigatorBoundingRect: DOMRect): ViewPortRect {
    return {
        top: styles.top as number,
        left: styles.left as number,
        bottom: styles.top as number + navigatorBoundingRect.height,
        right: styles.left as number + navigatorBoundingRect.width,
    };
}

export function createNavigatorPosStyles(args: CalculateNavigatorPosStylesArgs): CSSProperties | undefined {
    if (!args.navigatorElement || !args.highlightWindowElement) {
        return undefined;
    }
    const navigatorBoundingRect = args.navigatorElement.getBoundingClientRect();
    const highlightWindowBoundingRect = args.highlightWindowElement.getBoundingClientRect();

    let styles: CSSProperties = calculateNavigatorPosStyles(
        navigatorBoundingRect,
        highlightWindowBoundingRect,
        args.horizontalPosition,
        args.verticalPosition,
    );

    let rect: ViewPortRect = createViewPortRect(styles, navigatorBoundingRect);

    if (!isInViewport(rect)) {
        for (const config of navigatorPositionConfigs) {
            styles = calculateNavigatorPosStyles(
                navigatorBoundingRect,
                highlightWindowBoundingRect,
                config.horizontalPosition,
                config.verticalPosition,
            );

            rect = createViewPortRect(styles, navigatorBoundingRect);

            if (isInViewport(rect)) {
                return styles;
            }
        }
    }

    return styles;
}

export function createHighlightWindowPosStyles(elementToHighlight: HTMLElement): CSSProperties {
    const elementToHighlightBoundingRect = elementToHighlight.getBoundingClientRect();

    return {
        width: elementToHighlightBoundingRect.width,
        height: elementToHighlightBoundingRect.height,
        top: elementToHighlightBoundingRect.top + window.scrollY,
        left: elementToHighlightBoundingRect.left + window.scrollX,
    };
}

function isInViewport(rect: ViewPortRect): boolean {
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
