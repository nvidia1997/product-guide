import {HorizontalPosition, VerticalPosition} from "../../product-guide.typedef";
import {NavigatorPositionConfig} from "./guide.typedef";

export const elementOffset = 7;

export const navigatorPositionConfigs: NavigatorPositionConfig[] = [
    {
        horizontalPosition: HorizontalPosition.Left,
        verticalPosition: VerticalPosition.Bottom,
    },
    {
        horizontalPosition: HorizontalPosition.Left,
        verticalPosition: VerticalPosition.Top,
    },
    {
        horizontalPosition: HorizontalPosition.Right,
        verticalPosition: VerticalPosition.Bottom,
    },
    {
        horizontalPosition: HorizontalPosition.Right,
        verticalPosition: VerticalPosition.Top,
    },
    {
        horizontalPosition: HorizontalPosition.Left,
        verticalPosition: VerticalPosition.Center,
    },
    {
        horizontalPosition: HorizontalPosition.Right,
        verticalPosition: VerticalPosition.Center,
    },
];
