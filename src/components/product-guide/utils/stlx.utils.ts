import StringHelper from "./string-helper";

function _normalizeStyleName(styleName: string): string {
    const _words = styleName.split("-");

    for (let i = 0; i < _words.length; i++) {
        if (i === 0) {
            continue;
        }

        _words[i] = StringHelper.capitalize(_words[i]);
    }

    return _words.join("");
}

export function stlx(styles: { [key: string]: boolean }): object {
    if (typeof styles !== "object") {
        throw  new Error("invalid params");
    }

    const _allowedStyles = Object
        .keys(styles)
        .filter(key => styles[key] === true);

    const _style: any = {};
    for (const allowedStyle of _allowedStyles) {
        const _separatorIndex = allowedStyle.indexOf(":");
        if (_separatorIndex === -1) {
            continue;
        }

        let _propName = allowedStyle.slice(0, _separatorIndex);
        _propName = _normalizeStyleName(_propName);

        const _propValue = allowedStyle.slice(_separatorIndex + 1, allowedStyle.length);
        _style[_propName] = _propValue.replace(";", "");
    }


    return _style;
}

export default stlx;
