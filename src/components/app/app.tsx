import React from "react";
import {BrowserRouter as Router, Route, Switch,} from "react-router-dom";
import Home from "../../routes/home";

function App() {
    return (
        <Router>
            <div id="app">
                <Switch>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
