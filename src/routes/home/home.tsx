import React, {useState} from "react";
import ProductGuide, {VerticalPosition, HorizontalPosition} from "../../components/product-guide";

function Home() {
    const [isOpen, setIsOpen] = useState(true);

    return (
        <>
            <p id="item1">item 1</p>
            <p id="item11">item 1</p>
            <p>
                <span className="profile-text">Your profile picture:</span>

                <img
                    alt="cat"
                    src="https://timesofindia.indiatimes.com/photo/67586673.cms"
                    width={300}
                    height={150}
                    className="profile-picture"
                />
            </p>
            <p id="item4">item 4</p>
            <div id="item5" style={{height: 500}}>item 5</div>

            <ProductGuide
                isOpen={isOpen}
                onClose={() => setIsOpen(false)}
                verticalPosition={VerticalPosition.Center}
                horizontalPosition={HorizontalPosition.Right}
                steps={[
                    {
                        selector: "#item5",
                        content: () => <span>test content</span>,
                    },
                    {
                        title: "Very long texttttttttttttttttttttttttttttttttttttttttttttttt",
                        selector: ".profile-text",
                        content: "This one is used for example",
                    },
                    {
                        title: "Yeah, that's the step title",
                        selector: ".profile-picture",
                        content: "This is your profile picture",
                        action: ({stepIndex}) => {
                            // here you can do redirect to another page
                        }
                    }
                ]}
            />
        </>

    );
}

export default Home;
