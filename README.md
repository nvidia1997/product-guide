> Repository includes React plugin

[![Contributions welcome](https://img.shields.io/badge/contributions-welcome-orange.svg)](#contributing)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

<h1 align="center">
   Product Guide Widget
</h1>

![image info](https://fybielgm.filerobot.com/v7/DO_NOT_TOUCH_USED_FOR_PROD/plugins/product-guide/thumbnail-v0.0.2.png?vh=26c40d&ci_seal=ad8eb7d868)

## Usage:

```javascript
import ProductGuide, {VerticalPosition, HorizontalPosition} from "product-guide"; // there're also type exports
```

```javascript
     <ProductGuide
          isOpen={isOpen}
          onClose={() => setIsOpen(false)}
          verticalPosition={VerticalPosition.Center}
          horizontalPosition={HorizontalPosition.Right}
          steps={[
            {
              selector: "#item1",
              content: () => <span>test content</span>,
            },
            {
              title: "Very long texttttttttttttttttttttttttttttttttttttttttttttttt",
              selector: ".profile-text",
              content: "This one is used for example",
            },
            {
              title: "Yeah, that's the step title",
              selector: ".profile-picture",
              content: "This is your profile picture",
              action: ({stepIndex}) => {
                // here you can do redirect to another page
              }
            }
          ]}
    />
```

#### Done !

## Types:

[AppGuideStep](#AppGuideStep)
```typescript
type Props = {
    palette?: string, // main color theme for the widget
    steps: AppGuideStep[],
    isOpen: boolean,
    closeOnMaskClick?: boolean, // if true, the user can close the widget by clicking on the black mask
    onClose: () => void, // callback for clicking on the X (close) button
    verticalPosition?: VerticalPosition, // prefered vertical position (won't be used if there's no enough space)
    horizontalPosition?: HorizontalPosition, // prefered horizontal position (won't be used if there's no enough space)
    activeStepIndex?: number
    setActiveStepIndex?: (stepIndex: number) => void
}
```

[AppGuideStepActionArgs](#AppGuideStepActionArgs)
## <a id="AppGuideStep"></a>
```typescript
interface AppGuideStep {
    title?: any,
    selector?: string, // css selector like "#some-id" or ".some-class"
    content: any,
    action?: (args: AppGuideStepActionArgs) => void
}
```

## <a id="AppGuideStepActionArgs"></a>
```typescript
interface AppGuideStepActionArgs {
    stepIndex: number
}
```
