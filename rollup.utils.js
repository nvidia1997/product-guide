/**
 * @param {string} name
 * @param {string} filename
 * @param {string} css
 * @returns {string}
 */
export function generateScopedName(name, filename, css) {
  const _relativeFilename = filename.replace(`${__dirname}\\`, "");
  const hash = _hashCode(_relativeFilename);

  return `${_normalizeCssClassName(name)}-${hash}`;
}

/**
 * @author https://blog.trannhat.xyz/generate-a-hash-from-string-in-javascript/
 * @param {string} key
 * @returns {string}
 * @private
 */
function _hashCode(key) {
  const _hash = key
    .split("")
    .reduce(function (a, b) {
      a = ((a << 5) - a) + b.charCodeAt(0);
      return a & a;
    }, 0);

  return Math
    .abs(_hash)
    .toString()
    .substr(0, 5);
}

/**
 * @param {string} className
 * @returns {string}
 * @private
 */
function _normalizeCssClassName(className) {
  return className
    .split(/(?=[A-Z])/)
    .map(s => s.toLowerCase())
    .join("-");
}
